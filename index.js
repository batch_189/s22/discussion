//

//ARRAY METHODS

/**
    1. Mutator MEthods
        -Mutator methods are funstions that mutate or change an array
        after they're created. These method manipulate the original array
        performing various tasks such as adding and removing elements.
    

 */

    let fruits = ["apple", "orange", "kiwi", "guyabano"];

/**
 *   push()
 *        Add an element in the end of an array AND returns the array's length
 *      
 *         Syntax:
 *              arrayName.push(element)
 * 
 */

console.log("Current Array:");
console.log(fruits);

console.log();
// adding element, storing in variable
let fruitsLength = fruits.push("mango");
console.log("Result of push method:")
console.log(fruits)
fruits.push("avocado", "guava");
console.log(fruits);

/**
        pop()
            Removes the last element in our array AND returns the removed element

        Syntax:
            arrayName.pop();
 */
    console.log();
    let removedFruit = fruits.pop();
    console.log("Removes by pop()");
    console.log(removedFruit);
    console.log(fruits);

/**
        unshift()
            Adds one or more element at the beginning of an array

        Syntax:
            arrayName.unshift(elementA);
            arrayName.unshift(elementA, elementB);
 */

    fruits.unshift("lime", "banana");
    console.log("unshift method add element in front");
    console.log(fruits);

/**
        shift()
            Remove an element at the beginning of our array AND returns the
            removed element
        
        Syntax:
            arrayName.shift();
 */

    let anotherFruit = fruits.shift();
    console.log()
    console.log("shift method remove the front element");
    console.log(anotherFruit);
    console.log(fruits);

/**
        splice()
            Simultaneously removes elements from a specified index number and adds
            an element

        Syntax:
            arrayName.splice(startingIndex,)
 */

    fruits.splice(1, 2, "lime", "cherry");
    console.log()
    //let removedFruits = fruits.splice(1, 2, "lime", "cherry");
    // console.log(removedFruits)
    console.log("splice method (deleted the 1, 2 index, then add the lime, cherry)");
    console.log(fruits);

/**
        sort()
            Rearrange the array elements in alphanumeric order

        Syntax:
            arrayName.sort();
 */

    fruits.sort()
    console.log()
    console.log("sort method");
    console.log(fruits);
    console.log(fruits[0]);

    //sorting for number always 1st digit of the number if with string.
    let mixedArray = [50, 14, 100, "carlos", "nek", "bernard", "charles"];
    console.log(mixedArray.sort());

    let numbers = [2, 5, 7, 88];
    console.log();
    numbers = numbers.sort();
    console.log("sorting array of numbers alone:");
    console.log(numbers);
    
/**
        reverse()
            reverse the order of array elements
    
        Syntax:
            arrayName.reverse();
 */

    fruits.reverse();
    console.log()
    console.log("Reverse Method");
    console.log(fruits);


/**
    NON-MUTATOR METHODS
        -These are function that do not modify or change an array after they
        are created. These methods do not manipulate the original array
        performing various tasks sunch as returning element from an array and
        combining arrays and printing the output.
 */

    let countries = ["US", "PH", "CA", "SG", "TH", "PH", "FR", "DE"];

/**
        indexof()
            returns the index number of the firts meatching element found in an array.
            If no match was found, the result will be -1. The search
            process will be done from first element proceeding to the last element.

        Syntax:
            arrayName.indexOf(searchValue);
            arrayName.indeOf(searchValue, fromIndex);

 */

    // let firstIndex = countries.indexOf("PH", 4);
    // let firstIndex1 = countries.indexOf("PH", -1);
    // console.log()
    // console.log("indexOf method: " + firstIndex);
    // console.log(firstIndex1)


/**
        lastIndexOf();
            Returns the index number of the lasr matching element found in an
            array. The serach process will be done from last elemetn proceeding
            to the first eleemtn.

        Syntax:
            arrayName.lastIndexOf(searchValue);
            arrayName.lastIndexOf(searchValue, fromIndex);
 */

    let lastIndex = countries.lastIndexOf("PH");
    console.log()
    console.log("lastIndexOf: " + lastIndex)
    console.log()

    let lastIndexStart = countries.lastIndexOf("PH", 5);
    console.log("result of lastIndexOF: " + lastIndexStart);

/**
        slice();
            Portions/slices elements from an array AND retuens a new array

        Syntax:
            arrayName.slice(startIndex)
            arrayName.slice(startingIndex, endingIndex);
 */

    
 
    console.log();
    let sliceArrayA = countries.slice(2);
    console.log("result of slice method: ");
    console.log(sliceArrayA);
   
    console.log();
    let sliceArrayB = countries.slice(2, 4);
    console.log("result of slice method:");
    console.log(sliceArrayB);

    console.log();
    //  (-3) it count from the last element of the index
    let sliceArrayC = countries.slice(-3);
    console.log("result of slice method:");
    console.log(sliceArrayC);
    


/**
        toString();
            Returns an array as a string separated by commas

        Syntax:
            arrayName.toString();
 */

    let stringArray = countries.toString();
    console.log();
    console.log("result of toString:");
    console.log(stringArray);

/**
        concat()

 */

    let taskArrayA = ["drink html", "eat javascript"];
    let taskArrayB = ["inhale css", "breath sass"];
    let taskArrayC = ["get git", "be node"];

    let tasks = taskArrayA.concat(taskArrayB);
    console.log()
    console.log("result of concat:")
    console.log(tasks);

// COMBINATION MULTIPLE ARRAYS

    let allTasks = taskArrayA.concat(taskArrayB, taskArrayC);
    console.log(allTasks);

// COMBINING ARRAYS WITH ELEMENTS
    let combiningTasks = taskArrayA.concat("smell express", "throw react")
    console.log()
    console.log(combiningTasks);

/**
        join();
            returns an array as a string separator by specified separator string

        Syntax:
            arrayNAme.join(separatorString);
 */

    let students = ["tristan", "bernard", "carlos", "nehemiah"];
    console.log()
    console.log(students.join());
    console.log(students.join(' '));
    console.log(students.join(" - "));

/**
    ITERATION METHODS
        -are loops designeddd to perform erpetitive tasks on arrays.
         usefull for manipulation array data resulting in complex tasks.    
 */

/**
        forEach();
            similar to a for loop that iterated on each array element
        
        Syntax:
            arrayName.forEach( function (indivvElement) {
                statement
            })
 */

        allTasks.forEach(function(task) {
            console.log(task);
        });

    //Using forEach with conditional statement
        let filteredTasks = [];

        allTasks.forEach(function(task) {
            if (task.length > 10) {
                filteredTasks.push(task);
            }
        })
        console.log()
        console.log("result of forEach:");
        console.log(filteredTasks);

/**
        map();
            Iterates on each element AND returns new array with deiffernt values
            dependeing on the result og the function's operation.

        Syntax:
            let/const resultArray = arrayNAme.map(function(indivElement){
                statement
            })
 */

    let numbered = [1, 2, 3, 4, 5];

    let numberMap = numbered.map(function(number) {
        console.log(number);
        return number * number
    })
    console.log("original Array");
    console.log(numbers);
    console.log();
    console.log("result of map method");
    console.log(numberMap);

/**
        every();
            checks if all elements in an array met the given condition.
            returns a true value if all elements meet condition and false if otherwise.

        Syntax:
            let/const resultArray = arrayName.every(function())
 */
 
    let allValid = numbered.every(function(number) {
        return(number <3);
    })
    console.log();
    console.log("Result of every method");
    console.log(allValid);

/**
        some();
            checks if at least one element in the array meets the givven condition
            return a true value if at least one element meets the given condition
            and false if otherwise.

        Syntax:
            let/const resultArray = arrayName.some(function(indivElement) {
                return expression/condition
            })
 */

    let someValid = numbered.some(function(number) {
        return (number < 2);
    });
    console.log();
    console.log("result of some method");
    console.log(someValid);

/**
        filter();
            returns new array that contains elemetn which meet the given
            condition. return an empty array if no elements were found.

        Syntax:
            let/const resultArray = arrayName.filter(function(indivElement) {
                return expression/condition
            })
 */

    let filterValid = numbered.filter(function (number) {
        return (number < 3);
    })
    console.log();
    console.log("result of filter");
    console.log(filterValid);

    let nothingFound = numbered.filter(function (number) {
        return (number == 0);
    })
    console.log();
    console.log("result of another filter method");
    console.log(nothingFound);

// Filtering using forEach

    let filterNumbers = [];

    numbered.forEach(function(number) {
        if (number < 3) {
            filterNumbers.push(number);
        }
    });
    console.log();
    console.log("result of filtering using forEach");
    console.log(filterNumbers);

    let products = ["mouse", "keyboard", "laptop", "monitor"];
/**
        includes methods
            can be "chained" using them one after another.
            the result of the firsst method is used on the second method
            until all "cained" methods have been resolved.
 */

    let filterProducts = products.filter(function(product) {
        return product.toLowerCase().includes("a");
    });
    console.log();console.log();
    console.log("result of includes method");
    console.log(filterProducts);
    






    //(()), ()(), ((())), ()()(), ()
    // )(), ()(, ((()
    let parenthesis1 = ["(","(",")",")"];
    let parenthesis2 = ["(",")","(",")"];
    let parenthesis3 = ["(","(","(",")"];

    function isParenthesisValid(parenthesis) {
        let stack = [];
        for (i = 0; i < parenthesis.length; i++) {
        
            if ([1] == [2]) {
                
            }
        }
       
    }

    console.log(isParenthesisValid(parenthesis1));
    console.log(isParenthesisValid(parenthesis2));
    console.log(isParenthesisValid(parenthesis3));